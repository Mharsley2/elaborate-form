const express = require("express")
const path = require("path")

const app = express()
const publicFolderPath = path.join(__dirname, "public")

app.use(express.json())
app.use(express.static(publicFolderPath))

const users = []

// add POST request listener here

app.post('/api/user', (req, res) => {
    
    let taken = false
    
    for(user of users){
        if(user.username === req.body.username) {
            taken = true 
            break
        } else {
            taken = false
        }
    }
    if(taken) {
        res.status(409).send()
        throw new Error("username taken")
    } else {
        req.body.id=Math.random()
        users.push(req.body)
        res.status(201).send()
    }
    console.log(users)
})

app.listen(3000), ()=>{
    console.log('listening on port 3000')
}